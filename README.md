### Installation:

Require the extension using composer, then activate it.
```bash
composer config repositories.zwebbsrb/zwts git git@bitbucket.org:zwebbsrb/zwts.git
composer require zwebbsrb/zwts
```
